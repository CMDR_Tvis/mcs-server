rootProject.name = "mcs-server"

pluginManagement {
    val kotlinVersion: String by settings
    val shadowVersion: String by settings

    plugins {
        id("com.github.johnrengelman.shadow") version shadowVersion
        kotlin(module = "jvm") version kotlinVersion
    }
}
