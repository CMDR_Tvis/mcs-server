package io.github.commandertvis.logging

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.spi.LoggingEvent
import ch.qos.logback.core.filter.AbstractMatcherFilter
import ch.qos.logback.core.spi.FilterReply

class ConsoleFilter : AbstractMatcherFilter<Any?>() {
    override fun decide(event: Any?): FilterReply {
        if (!isStarted)
            return FilterReply.NEUTRAL

        return if ((event as LoggingEvent).level in eventsToKeep) {
            FilterReply.NEUTRAL
        } else {
            FilterReply.DENY
        }
    }

    private companion object {
        private val eventsToKeep = arrayOf<Level>(Level.INFO, Level.WARN, Level.ERROR)
    }
}
