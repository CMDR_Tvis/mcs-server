package io.github.commandertvis

import com.fasterxml.jackson.databind.JsonNode
import io.github.commandertvis.mcs.McsInterpreter
import io.github.commandertvis.model.Benchmark
import io.github.commandertvis.model.Plot
import io.github.commandertvis.model.Point
import io.ktor.application.call
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.post
import org.slf4j.LoggerFactory
import java.time.Duration
import kotlin.system.measureNanoTime

fun Route.functionPlotRoute(timeQuota: Duration) {
    val log = LoggerFactory.getLogger("FunctionPlotRoute")!!

    post(path = "plot") {
        if (requireJsonRequest())
            return@post

        call.response.header(name = HttpHeaders.Connection, value = "close")
        val request = call.receive<JsonNode>()

        val expression = request["expression"]?.textValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing string field \"expression\".")
            return@post
        }

        val fromInclusive = request["from_inclusive"]?.doubleValue() ?: run {
            call.respondText(
                status = HttpStatusCode.BadRequest,
                text = "Missing float field \"from_inclusive\"."
            )
            return@post
        }

        val toExclusive = request["to_exclusive"]?.doubleValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing float field \"to_exclusive\".")
            return@post
        }

        val nPoints = request["n_points"]?.longValue() ?: run {
            call.respondText(
                status = HttpStatusCode.BadRequest,
                text = "Missing integer field \"n_points\"."
            )

            return@post
        }

        val input = "expression=$expression, " +
                "fromInclusive=$fromInclusive, " +
                "toExclusive=$toExclusive, " +
                "nPoints=$nPoints"

        log.debug("Valid plot call: {}", input)

        val (time, result) = evaluateNativeOperationWithTimeoutAsync(timeQuota) {
            var plot: McsInterpreter.Result<Map<Double, Double>>? = null

            Duration.ofNanos(measureNanoTime {
                plot = McsInterpreter.plot(expression, fromInclusive, toExclusive, nPoints)
            })!! to plot
        }.await() ?: run {
            log.debug("Result of plot call: {} -> time quota exceeded - {}", input, timeQuota)

            call.respondText(
                status = HttpStatusCode.UnprocessableEntity,
                text = "Time quota exceeded. The allowed time quota is $timeQuota."
            )

            return@post
        }

        if (result == null || result.hasErrors()) {
            log.debug("Result of plot call: {} -> invalid formula", input)

            call.respond(
                status = HttpStatusCode.BadRequest,

                message = object {
                    val errors = result?.errors
                }
            )

            return@post
        }

        log.debug("Result of plot call: {} -> time={}, plot={}", input, time, result.value!!)

        call.respond(HttpStatusCode.OK, object {
            val benchmark =
                Benchmark(timePerCall = time.toNanos() / nPoints, calls = nPoints, totalTime = time.toMillis())

            val plot = Plot(result.value!!.map { (k, v) -> Point(k, v) })
        })
    }
}
