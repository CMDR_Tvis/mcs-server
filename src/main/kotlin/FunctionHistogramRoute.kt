package io.github.commandertvis

import com.fasterxml.jackson.databind.JsonNode
import io.github.commandertvis.mcs.McsInterpreter
import io.github.commandertvis.model.Benchmark
import io.github.commandertvis.model.Histogram
import io.ktor.application.call
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.post
import org.slf4j.LoggerFactory
import java.time.Duration
import kotlin.system.measureNanoTime

fun Route.functionHistogramRoute(timeQuota: Duration, workersQuantity: Int) {
    val log = LoggerFactory.getLogger("FunctionHistogramRoute")!!

    post(path = "histogram") {
        if (requireJsonRequest())
            return@post

        call.response.header(HttpHeaders.Connection, "close")
        val request = call.receive<JsonNode>()

        val expression = request["expression"]?.textValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing string field \"expression\".")
            return@post
        }

        val nValues = request["n_values"]?.longValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing integer field \"n_values\".")
            return@post
        }

        val xMin = request["x_min"]?.doubleValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing float field \"x_min\".")
            return@post
        }

        val xMax = request["x_max"]?.doubleValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing float field \"x_max\".")
            return@post
        }
        val histogramMin = request["histogram_min"]?.doubleValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing float field \"histogram_min\".")
            return@post
        }

        val histogramMax = request["histogram_max"]?.doubleValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing float field \"histogram_max\".")
            return@post
        }

        val nBins = request["n_bins"]?.longValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing integer field \"n_bins\".")
            return@post
        }

        val input = "expression=$expression, " +
                "nValues=$nValues, " +
                "xMin=$xMin, " +
                "xMax=$xMax, " +
                "histogramMin=$histogramMin, " +
                "histogramMax=$histogramMax, " +
                "nBins=$nBins"

        log.debug("Valid histogram call: {}", input)


        val (time, result) = evaluateNativeOperationWithTimeoutAsync(timeQuota) {
            var histogram: McsInterpreter.Result<List<Double>>? = null

            Duration.ofNanos(measureNanoTime {
                histogram = McsInterpreter(workersQuantity)
                    .use { it.histogram(expression, nValues, xMin, xMax, histogramMin, histogramMax, nBins) }
            })!! to histogram
        }.await() ?: run {
            log.debug("Result of histogram call: {} -> time quota exceeded - {}", input, timeQuota)

            call.respondText(
                status = HttpStatusCode.UnprocessableEntity,
                text = "Time quota exceeded. The allowed time quota is $timeQuota."
            )

            return@post
        }

        if (result == null || result.hasErrors()) {
            log.debug("Result of histogram call: {} -> invalid formula", input)

            call.respond(
                status = HttpStatusCode.BadRequest,

                message = object {
                    val errors = result?.errors
                }
            )

            return@post
        }

        log.debug("Result of histogram call: {} -> time={}, heights={}", input, time, result.value!!)

        call.respond(HttpStatusCode.OK, object {
            val benchmark =
                Benchmark(timePerCall = time.toNanos() / nValues, calls = nValues, totalTime = time.toMillis())

            val histogram = Histogram(result.value!!)
        })
    }
}
