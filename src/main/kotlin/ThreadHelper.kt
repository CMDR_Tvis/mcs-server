package io.github.commandertvis

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import java.time.Duration
import kotlin.concurrent.thread

@Suppress("DEPRECATION")
fun <R> CoroutineScope.evaluateNativeOperationWithTimeoutAsync(timeout: Duration, action: () -> R): Deferred<R?> {
    val acceptor = Channel<R?>(capacity = 1)
    val evaluator = thread { runBlocking { acceptor.send(action()) } }

    launch {
        delay(timeMillis = timeout.toMillis())

        if (evaluator.isAlive) {
            evaluator.stop()
            acceptor.send(null)
        }
    }

    return async { acceptor.receive() }
}
