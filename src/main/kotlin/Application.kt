package io.github.commandertvis

import io.github.commandertvis.jni.JNI
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.jackson.jackson
import io.ktor.request.path
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.cio.EngineMain
import org.slf4j.event.Level
import java.time.Duration

fun main(args: Array<String>): Unit = EngineMain.main(args)

fun Application.module() {
    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(CORS) {
        method(HttpMethod.Options)
        anyHost()
        allowCredentials = true
        allowNonSimpleContentTypes = true
        header(HttpHeaders.AccessControlAllowHeaders)
        header(HttpHeaders.ContentType)
        header(HttpHeaders.AccessControlAllowOrigin)
        header(HttpHeaders.Accept)
    }

    install(ContentNegotiation) { jackson {} }

    val timeQuota = Duration.ofMillis(
        environment.config.propertyOrNull("mcsServer.timeQuotaMillis")?.getString()?.toLongOrNull() ?: 60000L
    )!!

    val workersQuantity =
        environment.config.propertyOrNull("mcsServer.workersQuantity")?.getString()?.toIntOrNull() ?: 1

    JNI.loadLibraryFromJar(javaClass, path = "/libmcs2k.so")

    routing {
        route(path = "/function") {
            functionPlotRoute(timeQuota)
            functionHistogramRoute(timeQuota, workersQuantity)
            functionEvaluateRoute(timeQuota)
        }
    }
}
