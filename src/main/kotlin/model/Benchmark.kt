package io.github.commandertvis.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Benchmark(
    @JsonProperty("time_per_call") val timePerCall: Long,
    @JsonProperty("calls") val calls: Long,
    @JsonProperty("total_time") val totalTime: Long
)
