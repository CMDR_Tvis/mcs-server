package io.github.commandertvis.model

data class Plot(val points: List<Point>)
