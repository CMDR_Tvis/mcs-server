package io.github.commandertvis.model

data class Point(val x: Double, val y: Double)
