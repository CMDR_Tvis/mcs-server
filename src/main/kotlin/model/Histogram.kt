package io.github.commandertvis.model

data class Histogram(val heights: List<Double>)
