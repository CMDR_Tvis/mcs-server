package io.github.commandertvis

import com.fasterxml.jackson.databind.JsonNode
import io.github.commandertvis.mcs.McsInterpreter
import io.github.commandertvis.model.Benchmark
import io.ktor.application.call
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.post
import org.slf4j.LoggerFactory
import java.time.Duration
import kotlin.system.measureNanoTime

fun Route.functionEvaluateRoute(timeQuota: Duration) {
    val log = LoggerFactory.getLogger("FunctionEvaluateEndpoint")!!

    post(path = "evaluate") {
        if (requireJsonRequest())
            return@post

        call.response.header(HttpHeaders.Connection, "close")
        val request = call.receive<JsonNode>()

        val expression = request["expression"]?.textValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing string field \"expression\".")
            return@post
        }

        val x = request["x"]?.doubleValue() ?: run {
            call.respondText(status = HttpStatusCode.BadRequest, text = "Missing float field \"x\".")
            return@post
        }

        val input = "expression=$expression, x=$x"
        log.debug("Valid evaluate call: {}", input)

        val (time, result) = evaluateNativeOperationWithTimeoutAsync(timeQuota){
            var y: McsInterpreter.Result<Double>? = null

            Duration.ofNanos(measureNanoTime {
                y = McsInterpreter.evaluate(expression, x)
            })!! to y
        }.await() ?: run {
            log.debug("Result of evaluate call: {} -> time quota exceeded - {}", input, timeQuota)

            call.respondText(
                status = HttpStatusCode.UnprocessableEntity,
                text = "Time quota exceeded. The allowed time quota is $timeQuota."
            )

            return@post
        }

        if (result == null || result.hasErrors()) {
            log.debug("Result of evaluate call: {} -> invalid formula", input)

            call.respond(
                status = HttpStatusCode.BadRequest,

                message = object {
                    val errors = result?.errors
                }
            )

            return@post
        }

        log.debug(
            "Result of evaluate call: {} -> time={}, y={}",
            input,
            time,
            result.value!!
        )

        call.respond(HttpStatusCode.OK, object {
            val benchmark =
                Benchmark(timePerCall = time.toNanos(), calls = 1, totalTime = time.toMillis())

            val y = result.value!!
        })
    }
}
