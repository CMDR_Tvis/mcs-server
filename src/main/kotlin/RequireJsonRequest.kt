package io.github.commandertvis

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.contentType
import io.ktor.response.respondText
import io.ktor.util.pipeline.PipelineContext

suspend fun PipelineContext<Unit, ApplicationCall>.requireJsonRequest(): Boolean {
    if (call.request.contentType().contentType.contains("application/json")) {
        call.respondText(
            status = HttpStatusCode.BadRequest,
            text = "${ContentType.Application.Json} content-type expected."
        )

        return true
    }

    return false
}
