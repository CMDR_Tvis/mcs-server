val kotlinApiVersion: String by project
val kotlinJvmTarget: String by project
val kotlinLanguageVersion: String by project
val kotlinVersion: String by project
val ktorVersion: String by project
val logbackVersion: String by project
val mcs2kVersion: String by project
val mcsServerVersion: String by project
plugins { id("com.github.johnrengelman.shadow"); kotlin(module = "jvm") }
group = "io.github.commandertvis"
version = mcsServerVersion

repositories {
    jcenter()
    maven(url = "https://gitlab.com/api/v4/projects/11000558/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/17803458/packages/maven")
}

dependencies {
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("io.github.commandertvis:mcs2k:$mcs2kVersion")
    implementation("io.ktor:ktor-server-cio:$ktorVersion")
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-jackson:$ktorVersion")
    implementation(kotlin(module = "reflect", version = kotlinVersion))
    implementation(kotlin(module = "stdlib-jdk8", version = kotlinVersion))
}

tasks {
    compileKotlin.get().kotlinOptions {
        apiVersion = kotlinApiVersion
        freeCompilerArgs = listOf("-Xuse-experimental=io.ktor.util.KtorExperimentalAPI")
        jvmTarget = kotlinJvmTarget
        languageVersion = kotlinLanguageVersion
    }

    jar.get().manifest { attributes(mapOf("Main-Class" to "io.ktor.server.cio.EngineMain")) }

    shadowJar.get().minimize {
        exclude(dependency("io.ktor:.*:.*"))
        exclude(dependency("ch.qos.logback:logback-classic:.*"))
    }

    artifacts { archives(shadowJar) }
}
